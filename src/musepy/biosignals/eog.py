import numpy as np
from scipy import integrate
from biosppy import tools as biosppy_tools

LOG_RAW = False
LOG_FILTERED = False
LOG_INDEX = False

FILTER = True
CALC_EOG = True


class EOG:

    def __init__(self, window_size):
        self.window_size = window_size
        self.num_channels = 4
        self.sample_rate = 256.0
        # self.filter_freq = int(1)
        self.filter_freq = int(self.window_size * 1.0)

        self.send_freq = 1

        self.biosignals_npy = np.zeros((self.window_size, self.num_channels),
                                       dtype=np.float64)
        self.biosignals_filtered_npy = np.zeros((self.window_size, self.num_channels),
                                                dtype=np.float64)

        self.horizontal_eog_npy = np.zeros(self.num_channels,
                                           dtype=np.float64)
        self.vertical_eog_npy = np.zeros(self.num_channels,
                                         dtype=np.float64)

        self.last_heog = 0.0
        self.last_veog = 0.0

        self.h_eog_corr = 0.0
        self.v_eog_corr = 0.0

        self.curr_heog_sum = 0.0
        self.curr_veog_sum = 0.0

        self.sample_number = 0
        self.index = 0

    def process(self, l_ear, l_forehead, r_forehead, r_ear):
        self.set_index()

        self.update(l_ear, l_forehead, r_forehead, r_ear)

        send_l_ear, send_l_forehead, send_r_forehead, send_r_ear, \
        horizontal_eog, vertical_eog = self.prepare_eog_values(l_ear, l_forehead, r_ear, r_forehead)

        self.sample_number += 1

        return send_l_ear, send_l_forehead, send_r_forehead, send_r_ear, \
               horizontal_eog, vertical_eog

    def prepare_eog_values(self, l_ear, l_forehead, r_ear, r_forehead):

        if FILTER:
            self.filter_raw_data()

            send_l_ear = self.biosignals_filtered_npy[self.index, 0]
            send_l_forehead = self.biosignals_filtered_npy[self.index, 1]
            send_r_forehead = self.biosignals_filtered_npy[self.index, 2]
            send_r_ear = self.biosignals_filtered_npy[self.index, 3]

            # if self.sample_number >= self.window_size:

            if CALC_EOG:
                self.calc_eog()

            if CALC_EOG:
                try:
                    # horizontal_eog = self.horizontal_eog_npy[self.index]
                    # vertical_eog = self.vertical_eog_npy[self.index]

                    horizontal_eog = self.curr_heog_sum
                    vertical_eog = self.curr_veog_sum

                    # horizontal_eog = self.h_eog_corr
                    # vertical_eog = self.v_eog_corr
                except IndexError as e:
                    print(self.horizontal_eog_npy)
                    print(self.vertical_eog_npy)
                    print(self.biosignals_filtered_npy[:, 0])
                    print(self.biosignals_filtered_npy[:, 1])
                    print(self.biosignals_filtered_npy[:, 2])
                    print(self.biosignals_filtered_npy[:, 3])

                    print(self.biosignals_npy[:, 0])
                    print(self.biosignals_npy[:, 1])
                    print(self.biosignals_npy[:, 2])
                    print(self.biosignals_npy[:, 3])
                    raise e
            else:

                horizontal_eog, vertical_eog = self.simple_eog(send_l_ear, send_l_forehead, send_r_forehead,
                                                               send_r_ear)

                self.log_filtered_vals()
            # else:
            #     # send_l_ear = 0.0
            #     # send_l_forehead = 0.0
            #     # send_r_forehead = 0.0
            #     # send_r_ear = 0.0
            #
            #     horizontal_eog, vertical_eog = self.simple_eog(l_ear, l_forehead, r_forehead,
            #                                                    r_ear)
        else:
            send_l_ear = l_ear
            send_l_forehead = l_forehead
            send_r_forehead = r_forehead
            send_r_ear = r_ear

            horizontal_eog, vertical_eog = self.simple_eog(l_ear, l_forehead, r_forehead,
                                                           r_ear)
        horizontal_eog, vertical_eog = self.cutoff_eog(horizontal_eog, vertical_eog)
        return send_l_ear, send_l_forehead, send_r_forehead, send_r_ear, horizontal_eog, vertical_eog

    def update(self, l_ear, l_forehead, r_forehead, r_ear):

        log_msg = ""

        new_sample = np.zeros((1, 4), dtype=np.float64)
        new_sample[0, 0] = l_ear
        new_sample[0, 1] = l_forehead
        new_sample[0, 2] = r_forehead
        new_sample[0, 3] = r_ear

        if LOG_RAW:
            print("Raw sample: ", new_sample)

        log_msg += "Biosignals size before: " + str(len(self.biosignals_npy[:, 0])) + "\n"

        if self.sample_number < self.window_size:
            self.biosignals_npy[self.sample_number % self.window_size] = new_sample[0]
            log_msg += "Sample #: " + str(self.sample_number) + "\n"
            log_msg += "Sample # % window size: " + str(self.sample_number % self.window_size) + "\n"
        else:
            log_msg += str(new_sample) + "\n"
            self.biosignals_npy = np.append(self.biosignals_npy, new_sample, axis=0)
            log_msg += "Added biosignal: " + str(len(self.biosignals_npy[:, 0])) + "\n"

            while len(self.biosignals_npy[:, 0]) > self.window_size:
                self.biosignals_npy = np.delete(self.biosignals_npy, 0, axis=0)
                log_msg += "Deleted biosignal: " + str(len(self.biosignals_npy[:, 0])) + "\n"

        log_msg += "Biosignals size after: " + str(len(self.biosignals_npy[:, 0])) + "\n\n"

        # print(log_msg)

    def filter_raw_data(self):

        for channel in range(0, self.num_channels):
            filtered_data = self.filter(self.biosignals_npy[:, channel], self.window_size)

            self.biosignals_filtered_npy[:, channel] = filtered_data

    def filter(self, data, window_size):

        if self.sample_number <= 1:
            filtered_data = data - data
        elif 1 < self.sample_number < window_size:
            filtered_data = data - np.mean(data[:self.sample_number])
        else:
            filtered_data = data - np.mean(data)

            # np.multiply(filtered_data, np.hamming(self.window_size),
            #             out=filtered_data)

            b, a = biosppy_tools.get_filter(
                ftype='FIR',
                band='lowpass',
                order=8,
                frequency=20.0,
                sampling_rate=self.sample_rate)
            filtered_data, _ = biosppy_tools._filter_signal(b, a,
                                                            signal=filtered_data,
                                                            check_phase=True,
                                                            axis=0)

            # hamming_inverse = 1.0 / np.hamming(self.window_size)
            # np.multiply(filtered_data, hamming_inverse,
            #             out=filtered_data)

            if self.window_size > len(filtered_data):
                filtered_data = np.pad(
                    array=filtered_data,
                    pad_width=(window_size - len(filtered_data), 0),
                    mode='constant'
                )
                print("padded")
            if self.window_size < len(filtered_data):
                filtered_data = filtered_data[len(filtered_data) - window_size:]
                print("chopped")
        return filtered_data

    def calc_eog(self):
        l_ear = 0
        l_forehead = 1
        r_forehead = 2
        r_ear = 3

        self.horizontal_eog_npy = self.biosignals_filtered_npy[:, l_forehead] - self.biosignals_filtered_npy[:, r_forehead]
        self.vertical_eog_npy = -1.0 * (self.biosignals_filtered_npy[:, l_ear] + self.biosignals_filtered_npy[:, r_ear])
                                # + (self.biosignals_npy[:, l_forehead] + self.biosignals_npy[:, r_forehead])

        self.horizontal_eog_npy = self.filter(self.horizontal_eog_npy, self.window_size)
        self.vertical_eog_npy = self.filter(self.vertical_eog_npy, self.window_size)

        # Eye movement -> voltage changes in electrodes are correlated; noise -> voltage changes in electrodes aren't
        #   calculate distance between windows of electrodes, and threshold
        self.calc_eog_corr(l_ear, l_forehead, r_ear, r_forehead)

        # Integrate from (0,0) through all elements -- PROBLEMATIC!! REPLACE WITH SIMPLE ADDITION
        # self.horizontal_eog_npy = integrate.cumtrapz(y=self.horizontal_eog_npy,
        #                                              dx=1.0 / self.sample_rate,
        #                                              initial=self.last_heog)
        #
        # self.vertical_eog_npy = integrate.cumtrapz(y=self.vertical_eog_npy,
        #                                            dx=1.0 / self.sample_rate,
        #                                            initial=self.last_veog)

        # # Sum from (0,0) through all elements
        # self.horizontal_eog_npy = np.cumsum(self.horizontal_eog_npy) + self.last_heog
        # self.vertical_eog_npy = np.cumsum(self.vertical_eog_npy) + self.last_veog
        #
        # self.calc_integral_start()

        if self.sample_number >= self.window_size:

            if self.h_eog_corr < 2.0:
                self.curr_heog_sum += self.horizontal_eog_npy[self.index] / 256.0
            if self.v_eog_corr < 2.0:
                self.curr_veog_sum += self.vertical_eog_npy[self.index] / 256.0

            # # TODO: Instead, make sum tend to 0, more if smaller; less if larger -> tend to 0 more likely value is noise
            # self.subtract_from_abs(self.curr_heog_sum, 0.005)
            # self.subtract_from_abs(self.curr_veog_sum, 0.005)

        #
        # self.horizontal_eog_npy = np.divide(self.horizontal_eog_npy, self.sample_rate)
        # self.vertical_eog_npy = np.divide(self.vertical_eog_npy, self.sample_rate)

        # self.horizontal_eog_npy = self.logistic(self.horizontal_eog_npy)
        # self.vertical_eog_npy = self.logistic(self.vertical_eog_npy)

    def calc_eog_corr(self, l_ear, l_forehead, r_ear, r_forehead):
        # TODO (advice from Subash):
        # - take 40 trials of each gesture RANDOMLY (ie with a random number generator to indicate guesture) and plot
        #  them to see how correlated they are

        half_window = 10

        if self.index >= half_window:
            l_forehead_adj_window = self.biosignals_filtered_npy[self.index - half_window: self.index + half_window, l_forehead]
            r_forehead_adj_window = self.biosignals_filtered_npy[self.index - half_window: self.index + half_window, r_forehead]

            l_forehead_amplitude = np.max(l_forehead_adj_window) - np.min(l_forehead_adj_window)
            r_forehead_amplitude = np.max(r_forehead_adj_window) - np.min(r_forehead_adj_window)
            l_forehead_adj_window = np.divide(l_forehead_adj_window, l_forehead_amplitude)
            r_forehead_adj_window = np.divide(r_forehead_adj_window, r_forehead_amplitude)

            l_ear_adj_window = self.biosignals_filtered_npy[self.index - half_window: self.index + half_window, l_ear]
            r_ear_adj_window = self.biosignals_filtered_npy[self.index - half_window: self.index + half_window, r_ear]

            l_ear_amplitude = np.max(l_ear_adj_window) - np.min(l_ear_adj_window)
            r_ear_amplitude = np.max(r_ear_adj_window) - np.min(r_ear_adj_window)
            l_ear_adj_window = np.divide(l_ear_adj_window, l_ear_amplitude)
            r_ear_adj_window = np.divide(r_ear_adj_window, r_ear_amplitude)

            h_eog_recentered = l_forehead_adj_window + r_forehead_adj_window
            h_eog_recentered -= np.mean(h_eog_recentered)

            v_eog_recentered = l_ear_adj_window - r_ear_adj_window
            v_eog_recentered -= np.mean(v_eog_recentered)

            self.h_eog_corr = np.linalg.norm(h_eog_recentered)
            self.v_eog_corr = np.linalg.norm(v_eog_recentered)

            # self.h_eog_corr = np.mean(np.abs(h_eog_recentered))
            # self.v_eog_corr = np.mean(np.abs(v_eog_recentered))

    def log_filtered_vals(self):
        if LOG_FILTERED:
            print("Filtered L_ear: ", self.biosignals_filtered_npy[self.index, 0],
                  " L_forehead: ", self.biosignals_filtered_npy[self.index, 1],
                  " R_forehead: ", self.biosignals_filtered_npy[self.index, 2],
                  " R_ear: ", self.biosignals_filtered_npy[self.index, 3])

    def calc_integral_start(self):

        self.last_heog = self.horizontal_eog_npy[0]
        self.last_veog = self.vertical_eog_npy[0]

        self.subtract_from_abs(self.last_heog, self.last_heog * 0.001)
        self.subtract_from_abs(self.last_veog, self.last_veog * 0.001)

    def set_index(self):
        if self.sample_number == self.window_size * 1000:
            self.sample_number = self.window_size

        # if self.sample_number >= self.window_size - self.filter_freq:
        #     if self.index >= self.window_size - 1:
        #         self.index = self.window_size - self.filter_freq - 1
        #     else:
        #         self.index += 1
        if self.sample_number < self.window_size - 10:
            self.index = self.sample_number
        else:
            # self.index = self.sample_number % self.window_size
            self.index = self.window_size - 10

        if LOG_INDEX:
            print(self.index)

    def simple_eog(self, l_ear, l_forehead, r_forehead, r_ear) -> (float, float):
        """
        Horizontal eog: left is -ve, right is +ve
        Vertical eog: bottom is -ve, top is +ve

        :param l_ear: float Left ear voltage
        :param l_forehead: float Left forehead voltage
        :param r_forehead: float Right forehead voltage
        :param r_ear: float Right ear voltage

        :return: (horizontal_eog, vertical_eog)
        """

        horizontal_eog = l_forehead - r_forehead
        vertical_eog = -1 * (l_ear + r_ear)

        horizontal_eog = self.logistic(horizontal_eog)
        vertical_eog = self.logistic(vertical_eog)

        return horizontal_eog, vertical_eog

    @staticmethod
    def logistic(nparray):
        L = 2.0
        k = 0.2
        x_0 = 0.0

        return L / (1 + np.exp(-k * (nparray - x_0))) - 1.0

    @staticmethod
    def subtract_from_abs(number, subtract):
        if number > 0:
            number -= abs(subtract)
        else:
            number += abs(subtract)

    @staticmethod
    def cutoff_eog(horizontal_eog, vertical_eog):
        horizontal_eog = horizontal_eog if not np.isnan(horizontal_eog) and abs(horizontal_eog) < 1000.0 else None
        vertical_eog = vertical_eog if not np.isnan(vertical_eog) and abs(vertical_eog) < 1000.0 else None
        return horizontal_eog, vertical_eog
