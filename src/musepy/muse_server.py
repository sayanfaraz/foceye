import argparse

from pythonosc import dispatcher
from pythonosc import osc_server

from signal_handler.eog_handler import EOGHandler


def muse_handler(unused_addr, args, l_ear, l_forehead, r_forehead, r_ear, ch5, ch6):
    """
    Executes actions on OSC sample received from HTTP port (unused_addr).
    """
    # biosignal = args[0]
    # horizontal_eog, vertical_eog = biosignal.process(l_ear, l_forehead, r_forehead, r_ear)
    # print("EOG (uV): ", horizontal_eog, vertical_eog)
    print("EEG (uV) per channel: ", l_ear, l_forehead, r_forehead, r_ear, ch5, ch6)
    # biosignal.process(unused_addr, args, l_ear, l_forehead, r_forehead, r_ear, ch5, ch6)


if __name__ == "__main__":
    SOCKET_PORT = 8001

    parser = argparse.ArgumentParser()
    parser.add_argument("--ip",
                        default="127.0.0.1",
                        help="The ip to listen on")
    parser.add_argument("--port",
                        type=int,
                        default=7000,
                        help="The port to listen on")
    parser.add_argument("--address",
                        type=str,
                        default="/muse/eeg",
                        help="The OSC address to listen on")
    args = parser.parse_args()

    eog = EOGHandler(port=SOCKET_PORT,
                     window_size=512)

    dispatcher = dispatcher.Dispatcher()
    # dispatcher.map(args.address, muse_handler, "EOG")
    dispatcher.map(args.address, eog.handler, "eog")

    server = osc_server.ThreadingOSCUDPServer(
        (args.ip, args.port), dispatcher)
    print("Serving on {}".format(server.server_address))
    server.serve_forever()
