import asyncio
import logging

import aiouv
import pyuv

from sanic import Sanic
import socketio

sio = socketio.AsyncServer()
app = Sanic()
sio.attach(app)

logging.getLogger('socketio').setLevel(logging.ERROR)
logging.getLogger('engineio').setLevel(logging.ERROR)


# asyncio.set_event_loop_policy(aiouv.EventLoopPolicy())

@sio.on('server_eog')
async def message(sid, data):
    print("EOG: ", data)
    await sio.emit("eye_event", data)

#
# @sio.on('muse_server')
# async def message(sid, data):
#     print("EOG: ", data)
#     await sio.emit("data_event", data)


if __name__ == '__main__':
    app.run(host='localhost', port=8001)
