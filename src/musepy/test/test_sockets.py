from quart import Quart

app = Quart(__name__)


async def test_websocket():
    test_client = app.test_client()
    data = b'bob'
    with test_client.websocket('/main') as test_websocket:
        test_websocket.send(data)
        result = await test_websocket.receive()
        print("Message sent!")

if __name__ == '__main__':
    test_websocket()
