"""Small example OSC client
This program sends 10 random values between 0.0 and 1.0 to the /filter address,
waiting for 1 seconds between each value.
"""
import argparse
import csv
import json
import random
import time

from pythonosc import osc_message_builder
from pythonosc import udp_client


def random_number():
    return (random.random() - 0.5) * 20


def random_output(client):
    while True:
        client.send_message("/muse/eeg",
                            [random_number(),
                             random_number(),
                             random_number(),
                             random_number(),
                             random_number(),
                             random_number()])
        time.sleep(1.0 / 256.0)


def data_has_nulls(data):
    for key in ['l_ear', 'l_forehead', 'r_forehead', 'ch6']:
        point = data[key]
        if point is None or point == "" or point == "nan":
            return True
    return False


def output_from_file(client, filename):
    with open(filename, "r") as eog_file:
        eog_reader = csv.DictReader(eog_file)

        while True:
            for data in eog_reader:

                if not data_has_nulls(data):
                    client.send_message("/muse/eeg",
                                        # [float(data['l_ear']),
                                        #  float(data['l_forehead']),
                                        #  float(data['r_forehead']),
                                        [0,0,0,
                                         float(data['ch6']),
                                         0,
                                         0])

                time.sleep(1.0 / 256.0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", default="127.0.0.1",
                        help="The ip of the OSC server")
    parser.add_argument("--port", type=int, default=7000,
                        help="The port the OSC server is listening on")
    args = parser.parse_args()

    client = udp_client.SimpleUDPClient(args.ip, args.port)

    output_from_file(client, "./out/2018-9-18_11h55m14s.csv")
