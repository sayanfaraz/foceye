"""Small example OSC client
This program sends 10 random values between 0.0 and 1.0 to the /filter address,
waiting for 1 seconds between each value.
"""
import argparse
import random
import time

from pythonosc import osc_message_builder
from pythonosc import udp_client


def random_number():
    return (random.random() - 0.5) * 20


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", default="127.0.0.1",
                        help="The ip of the OSC server")
    parser.add_argument("--port", type=int, default=7000,
                        help="The port the OSC server is listening on")
    args = parser.parse_args()

    client = udp_client.SimpleUDPClient(args.ip, args.port)

    #   for x in range(10):
    while True:
        client.send_message("/muse/eeg",
                            [random_number(),
                             random_number(),
                             random_number(),
                             random_number(),
                             random_number(),
                             random_number()])
        time.sleep(1.0 / 256.0)
#     client.send_message("/muse-white/eeg",
#                         [3,
#                          1,
#                          1,
#                          1,
#                          1,
#                          1])
