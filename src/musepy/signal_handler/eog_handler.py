import csv
import json
import time
import random
import websocket

import numpy as np

from socketIO_client import SocketIO
from datetime import datetime

################################################ FEATURE FLAGS ################################################
from biosignals.eog import EOG

SOCKET_ON = False
SAVE_DATA = True


class EOGHandler:

    def __exit__(self, exc_type, exc_val, exc_tb):
        if SAVE_DATA:
            self.save_file.__exit__(None, None, None)

    def __init__(self, port: int = 8000, window_size: int = 256):
        self.skip_samples = 1

        self.eog = EOG(window_size)

        if SAVE_DATA is True:
            now = datetime.now()
            now_filename = str(now.year) + "-" + str(now.month) + "-" + str(now.day) + "_" \
                           + str(now.hour) + "h" + str(now.minute) + "m" + str(now.second) + "s"
            self.save_file = open("./out/" + now_filename + ".csv", "w")
            self.fieldnames = ["l_ear",
                               "l_forehead",
                               "r_forehead",
                               "r_ear",
                               "ch5",
                               "ch6"]
            self.csv_writer = csv.DictWriter(self.save_file, self.fieldnames)
            self.csv_writer.writeheader()

        self.port = port
        if SOCKET_ON:
            self.socket_client = SocketIO("localhost", self.port)
            self.socket_client.connect()
            # self.__socket_event__ = "muse_server"
            self.__socket_event__ = "server_eog"

    def handler(self, unused_addr, args, l_ear, l_forehead, r_forehead, r_ear, ch5, ch6):
        """
        Executes actions on OSC sample received from HTTP port (unused_addr).
        """

        # print("EOG (uV): ", horizontal_eog, vertical_eog)
        # print("EEG (uV) per channel: ", l_ear, l_forehead, r_forehead, r_ear, ch5, ch6)

        if self.eog.sample_number % self.skip_samples == 0:

            print_msg = {
                "l_ear": l_ear,
                "l_forehead": l_forehead,
                "r_forehead": r_forehead,
                "r_ear": r_ear,
                "ch5": ch5,
                "ch6": ch6
            }

            # print(print_msg)

            if SAVE_DATA:
                self.csv_writer.writerow(print_msg)

            if np.isnan(l_ear) or np.isnan(l_forehead) or np.isnan(r_forehead) or np.isnan(r_ear):
                send_l_ear = None
                send_l_forehead = None
                send_r_forehead = None
                send_r_ear = None
                horizontal_eog = None
                vertical_eog = None
            else:
                send_l_ear, send_l_forehead, send_r_forehead, send_r_ear, \
                    horizontal_eog, vertical_eog = self.eog.process(l_ear, l_forehead, r_forehead, r_ear)

            eeg_msg = {
                "l_ear": send_l_ear,
                "l_forehead": send_l_forehead,
                "r_forehead": send_r_forehead,
                "r_ear": send_r_ear,
                "h_eog": horizontal_eog,
                "v_eog": vertical_eog
            }

            eeg_json = json.dumps(eeg_msg)
            # print(eeg_json)

            if SOCKET_ON and self.eog.sample_number % self.eog.send_freq == 0:
                self.socket_client.emit(self.__socket_event__, eeg_json)

    if __name__ == '__main__':
        socket_client = SocketIO("localhost", 8001)
        socket_client.connect()

        while True:
            eeg_msg = {
                "l_ear": random.random(),
                "l_forehead": random.random(),
                "r_forehead": random.random(),
                "r_ear": random.random(),
                "h_eog": random.random(),
                "v_eog": random.random()
            }

            eeg_json = json.dumps(eeg_msg)
            # print(eeg_json)

            socket_client.emit("server_eog", eeg_json)
            time.sleep(1.0 / 10.0)

        # eog = EOG()
        # eog.update(1, 2, 3, 4)
        # eog.update(1, 2, 3, 4)
