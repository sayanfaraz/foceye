export class Biosignals {
    public l_ear: number[];
    public l_forehead: number[];
    public r_forehead: number[];
    public r_ear: number[];

    public h_eog: number[];
    public v_eog: number[];

    public length: number;
    public maxLength: number;

    constructor(public overLengthLimit: number) {
        this.l_ear = [0,0];
        this.l_forehead = [0,0];
        this.r_forehead = [0,0];
        this.r_ear = [0,0];

        this.h_eog = [0,0];
        this.v_eog = [0,0];

        // this.l_ear = [];
        // this.l_forehead = [];
        // this.r_forehead = [];
        // this.r_ear = [];

        this.length = 2;
        this.maxLength = 256;
    }

    public addBiosignal(datapoint) {
        // if (this.length - this.maxLength > this.overLengthLimit) {
        //     this.removeBiosignal(0, this.overLengthLimit);
        // }

        if (this.length > this.maxLength) {
            this.trim()
        }

        this.l_ear.push(datapoint.l_ear);
        this.l_forehead.push(datapoint.l_forehead);
        this.r_forehead.push(datapoint.r_forehead);
        this.r_ear.push(datapoint.r_ear);

        // this.h_eog.push(datapoint.h_eog);
        // this.v_eog.push(datapoint.v_eog);

        this.length += 1;
    }

    public getBiosignal(index: number) {
        return {
            "l_ear": this.l_ear[index],
            "l_forehead": this.l_forehead[index],
            "r_forehead": this.r_forehead[index],
            "r_ear": this.r_ear[index],

            // "h_eog": this.h_eog[index],
            // "v_eog": this.v_eog[index],
        };
    }

    public trim() {
        this.l_ear.shift();
        this.l_forehead.shift();
        this.r_forehead.shift();
        this.r_ear.shift();

        // this.h_eog.shift();
        // this.v_eog.shift();
    }

    public removeBiosignal(index: number, numOfSamples: number) {
        this.l_ear.splice(index, numOfSamples);
        this.l_forehead.splice(index, numOfSamples);
        this.r_forehead.splice(index, numOfSamples);
        this.r_ear.splice(index, numOfSamples);

        // this.h_eog.splice(index, numOfSamples);
        // this.v_eog.splice(index, numOfSamples);

        this.length -= numOfSamples;
    }
}
