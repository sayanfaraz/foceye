import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";

import { CommonModule } from '@angular/common';

import { BiofeebackComponent } from "./biofeeback/biofeeback.component";
import { HomeComponent } from './home/home.component';
import { DevGraphComponent } from './dev-graph/dev-graph.component';

const routes: Routes = [
    { path: '',  redirectTo: '/home', pathMatch: 'full'},
    { path: 'home', component: HomeComponent},
    { path: 'start', component: BiofeebackComponent },
    { path: 'dev', component: DevGraphComponent }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
