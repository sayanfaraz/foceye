import { Component, OnInit } from '@angular/core';

// import * as anime from 'animejs';
import { PaperScope, Project, IFrameEvent } from "paper";
import { Subscription } from 'rxjs';

import { Circle } from "./circle/circle";
import { FocusCircle } from "./circle/focus-circle";
import { SocketService } from "../socket.service";
import { SocketEvent } from "../socket-events";
import { EyeCircle } from './circle/eye-circle';

@Component({
    selector: 'app-biofeeback',
    templateUrl: './biofeeback.component.html',
    styleUrls: ['./biofeeback.component.css']
})
export class BiofeebackComponent implements OnInit {

    ioSubscription: Subscription;

    public canvas: HTMLCanvasElement;
    public paragraph: HTMLParagraphElement;
    public canvasCtx: CanvasRenderingContext2D;

    public paperProject: Project;
    public paperScope: PaperScope;

    public focusCircle: FocusCircle;

    private littleRadius: number = 100;
    private bigRadius: number = 150;

    private centerX: number;
    private centerY: number;

    private circleArray: Array<EyeCircle> = [];
    private circlesBorderRadius: number;
    private eyeCircleRadius: number = 20;

    private FOCUS_CIRCLE_ON: boolean = true;

    constructor(private socketService: SocketService) {
        this.centerX = document.body.clientWidth / 2.0;
        this.centerY = window.innerHeight / 2.0;

        this.circlesBorderRadius = Math.min(document.body.clientWidth, window.innerHeight) / 2.0;
    }

    ngOnInit() {
        this.makeCanvas();
        this.initPaper();

        if (this.FOCUS_CIRCLE_ON) {
            this.focusCircle = new FocusCircle(
                this.centerX,
                this.centerY,
                this.littleRadius,
                "#c1afe2"
            );
        }

        this.initSocketConnection();
    }

    ngAfterViewInit() {}

    private initSocketConnection(): void {
        this.socketService.initSocket();

        this.ioSubscription = this.socketService.onData(SocketEvent.EYE_EVENT).subscribe(
            (datapoint: string) => {
                this.handleData(datapoint);
            }
        )
    }

    private handleData(datapoint: string) {
        let datapointJson = JSON.parse(datapoint);

        let center = this.calcEyeCircleCenter(datapointJson)

        if (center.r > this.littleRadius) {
            let circle = new EyeCircle(
                center.x,
                center.y,
                this.eyeCircleRadius,
                "#c1afe2"
            )
            console.log("Center: " + JSON.stringify(center));
            this.circleArray.push(circle);
        }
    }

    private calcEyeCircleCenter(datapointJson) {

        let angle: number = Math.atan2(datapointJson.v_eog, datapointJson.h_eog)

        let xOffset: number = this.littleRadius * Math.cos(angle)
        let yOffset: number = this.littleRadius * Math.sin(angle)

        let x:number = this.centerX + datapointJson.h_eog * this.circlesBorderRadius + xOffset;
        let y:number = this.centerY - datapointJson.v_eog * this.circlesBorderRadius - yOffset;
        let r:number = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

        return {
            x: x,
            y: y,
            r: r
        };
    }

    makeCanvas() {
        this.canvas = <HTMLCanvasElement>document.getElementById('canvas');
        this.paragraph = <HTMLParagraphElement>document.getElementById('p');
        this.canvas.width = document.body.clientWidth;
        this.canvas.height = document.body.clientHeight;{}
        this.canvasCtx = this.canvas.getContext("2d");
    }

    initPaper() {
        this.paperScope = new PaperScope();
        this.paperScope.activate();

        this.paperProject = new Project(this.canvas);

        this.paperScope.view.onFrame = (event: IFrameEvent) => {
            this.paperOnFrame(event);
        }
        this.paperScope.view.draw();
    }

    makeCircle(circle: Circle) {
        circle.draw(this.canvasCtx);
    }

    paperOnFrame(event: IFrameEvent) {
        if (this.FOCUS_CIRCLE_ON) {
            this.focusCircle.growShrink();
        }

        this.animateEyeCircles();
    }

    animateEyeCircles() {
        for (let id = 0; id < this.circleArray.length; id++) {
            const circle = this.circleArray[id];
            circle.animate();

            if (circle.destroy) {
                console.log("Circles: " + this.circleArray.length);
                this.circleArray.splice(id, 1);
                console.log("Destroying circle " + id +
                 " new length: " + this.circleArray.length);
            }
        }
    }
}
