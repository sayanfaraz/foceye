import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BiofeebackComponent } from './biofeeback.component';

describe('BiofeebackComponent', () => {
  let component: BiofeebackComponent;
  let fixture: ComponentFixture<BiofeebackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BiofeebackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiofeebackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
