import { Circle } from "./circle";

export class FocusCircle extends Circle {

    private isGrowOrShrink: boolean = true;
    private isPause: boolean = false;
    private growFactor: number = 1.007;
    private shrinkFactor: number = 0.999;

    private frame: number = 0;
    private SKIP_FRAMES: number = 4;

    private lastScaleSwitchTime: number = Date.now();
    private SCALE_DURATION: number = 4500.0;
    private PAUSE_DURATION: number = 2000.0;

    constructor(
        public centerX: number,
        public centerY: number,
        public radius: number,
        public color: string
    ) {
        super(centerX, centerY, radius, color);
    }

    public growShrink() {
        if (this.frame % this.SKIP_FRAMES == 0) {
            this.frame = 0;

            let now: number = Date.now();

            if (!this.isPause && (now - this.lastScaleSwitchTime) >= this.SCALE_DURATION) {
                this.isPause = true;
                this.lastScaleSwitchTime = now;
            }
            else if (this.isPause && (now - this.lastScaleSwitchTime) >= this.PAUSE_DURATION) {
                this.isPause = false;
                this.isGrowOrShrink = !this.isGrowOrShrink;

                this.lastScaleSwitchTime = now;
            }

            if (this.isPause) {
                this.paperCircle.scale(1);
            }
            else if (this.isGrowOrShrink) {
                this.paperCircle.scale(this.calcGrow(now, this.lastScaleSwitchTime));
            }
            else {
                this.paperCircle.scale(this.calcShrink(now, this.lastScaleSwitchTime));
            }
        }

        this.frame++;
    }

    public calcGrow(now:number, lastScaleSwitchTime:number) {
        // let growFactor:number = this.focusCircleGrowFactor * (this.centeredNormal(now - lastScaleSwitchTime, this.scaleDuration) + 1);

        // let growFactor:number = this.growFactor * ((
        //     (now - lastScaleSwitchTime) / this.SCALE_DURATION) ^ 2.0
        // ) / 2.0;

        // let growFactor:number = this.growFactor * (Math.log(
        //     (now - lastScaleSwitchTime) / (this.SCALE_DURATION * 40) + Math.E)
        // );

        let growFactor:number = this.growFactor - ((this.growFactor - 1) / this.SCALE_DURATION) * (now - lastScaleSwitchTime);

        return growFactor;
    }

    public calcShrink(now:number, lastScaleSwitchTime:number) {
        // let shrinkFactor = this.focusCircleShrinkFactor * (this.centeredNormal(now - lastScaleSwitchTime, this.scaleDuration) + 1);

        let shrinkFactor:number = 1 / this.calcGrow(now, lastScaleSwitchTime);

        // console.log("Shrinking by: ", shrinkFactor);
        return shrinkFactor;
    }

    public centeredNormal(x:number, std:number) {
        return Math.exp(-0.5 * x^2 / (2 * std^2)) / Math.sqrt(2 * Math.PI * std^2);
    }
}
