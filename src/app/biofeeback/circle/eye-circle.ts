import { Circle } from "./circle";

export class EyeCircle extends Circle {

    public destroy: boolean = false;

    private shrinkFactor: number = 0.98;

    constructor(
        public centerX: number,
        public centerY: number,
        public radius: number,
        public color: string
    ) {
        super(centerX, centerY, radius, color);
    }

    animate() {
        this.shrink();
    }

    make() {

    }

    shrink() {
        this.radius *= this.shrinkFactor;

        if (this.radius <= 0.05) {
            this.destroy = true;
        }

        this.paperCircle.scale(this.shrinkFactor);
    }

    gravitate(centerX: number, centerY: number) {

    }

    applyGravity() {

    }
}
