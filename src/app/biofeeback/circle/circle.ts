import * as anime from 'animejs';
import { Path, Point } from "paper";

export class Circle {

    public paperCircle: Path.Circle;

    constructor(
        public centerX: number,
        public centerY: number,
        public radius: number,
        public color: string
    ) {
        this.paperCircle = new Path.Circle(
            new Point(this.centerX, this.centerY),
            this.radius,
        );
        this.paperCircle.fillColor = color;
    }

    /**
     * Draw circle, given Canvas context.
     */
    public draw(canvasCtx: CanvasRenderingContext2D) {
        requestAnimationFrame(() => this.draw(canvasCtx));
        canvasCtx.beginPath();
        canvasCtx.arc(this.centerX, this.centerY,
            this.radius, 0, 2*Math.PI);
        canvasCtx.fillStyle = this.color;
        canvasCtx.fill();

        canvasCtx.closePath();
    }

    /**
     * Animates resizing of circle.
     * @param targetRadius Target radius
     * @param duration Animation duration (ms)
     */
    resizeCircle(targetRadius: number, duration: number, canvasCtx: CanvasRenderingContext2D) {
        let radiusStep: number = (this.radius - targetRadius) / duration;

        for (var i: number = 0; i < duration; i++) {
            this.radius += radiusStep;
            this.draw(canvasCtx);
        }
    }
}
