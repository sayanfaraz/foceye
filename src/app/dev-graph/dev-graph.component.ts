import { Component, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';
import { IChartistLineChart } from "chartist";
import * as Chartist from "chartist";
import "chartist-plugin-legend";
import * as d3 from 'd3';

import { SocketService } from "../socket.service";
import { SocketEvent } from "../socket-events";
import { Biosignals } from "../biosignals";

@Component({
    selector: 'app-dev-graph',
    templateUrl: './dev-graph.component.html',
    styleUrls: ['./dev-graph.component.css']
})
export class DevGraphComponent implements OnInit {

    private socketService: SocketService;

    private biosignals: Biosignals;
    ioSubscription: Subscription;

    public canvas: HTMLCanvasElement;
    public canvasCtx: CanvasRenderingContext2D;

    public chart: IChartistLineChart;
    public lastItem: string;

    private shouldRender: number = 0;
    private renderFreq: number = 40;

    constructor() {
        this.socketService = new SocketService();
        this.biosignals = new Biosignals(this.renderFreq);
    }

    ngOnInit() {
        this.makeChart();
        this.initSocketConnection();
    }

    private initSocketConnection(): void {
        this.socketService.initSocket();

        this.ioSubscription = this.socketService.onData(SocketEvent.EYE_EVENT).subscribe(
            (datapoint: string) => {
                this.handleData(datapoint)
            }
        )
    }

    private handleData(datapoint: string) {
        // this.lastItem = datapoint;
        // console.log(datapoint);
        if (this.shouldRender % 2 == 0) {
            let datapointJson = JSON.parse(datapoint);
            this.biosignals.addBiosignal(datapointJson);
            this.shouldRender += 1;

            if (this.shouldRender == this.renderFreq) {
                this.updateChart();
                this.shouldRender = 0;
            }
        }
    }

    private makeChart() {
        this.chart = new Chartist.Line(
            '.ct-chart',
            { series: [
                this.biosignals.l_ear,
                this.biosignals.l_forehead,
                this.biosignals.r_forehead,
                this.biosignals.r_ear
                // this.biosignals.h_eog,
                // this.biosignals.v_eog
            ] },
            {
                width: document.body.clientWidth,
                height: document.body.clientHeight,
                plugins: [
                    Chartist.plugins.legend({
                        position: 'bottom',
                        legendNames: ['Left ear', 'Left forehead', 'Right forehead', 'Right ear'],
                        // legendNames: ['Horizontal EOG', 'Vertical EOG'],
                     })
                ],
                low: 500,
                high: 1000
            }
        );
    }

    private updateChart() {
        this.chart.update({
            series: [
                // this.biosignals.l_ear,
                // this.biosignals.l_forehead,
                // this.biosignals.r_forehead,
                // this.biosignals.r_ear
                // this.biosignals.h_eog,
                // this.biosignals.v_eog
            ]
        })
    }
}
