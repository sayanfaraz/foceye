import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class SocketService {

    public socket;
    private SOCKET_URL: string = "http://localhost:8001";
    // private DATA_EVENT: string = 'data_event';

    constructor() { }

    public initSocket(): void {
        this.socket = io(this.SOCKET_URL);
    }

    public send(message: string) {
        this.socket.emit('message', message);
    }

    public onData(event: String) {
        return new Observable<string> (observer => {
            this.socket.on(
                event,
                (data: string) => observer.next(data)
            )
        });
    }

    // /**
    //  * connect
    //  */
    // public connect(): Subject<MessageEvent> {
    //     // If you aren't familiar with environment variables then
    //     // you can hard code `environment.ws_url` as `http://localhost:5000`


    //     // We define our observable which will observe any incoming messages
    //     // from our socket.io server.
    //     let observable = new Observable(observer => {
    //         this.socket.on('data_event', (data) => {
    //             console.log("Received message from Websocket Server")
    //             observer.next(data);
    //         })
    //         return () => {
    //             this.socket.disconnect();
    //         }
    //     });

    //     // We define our Observer which will listen to messages
    //     // from our other components and send messages back to our
    //     // socket server whenever the `next()` method is called.
    //     let observer = {
    //         next: (data: Object) => {
    //             this.socket.emit('message', JSON.stringify(data));
    //         },
    //     };

    //     // we return our Rx.Subject which is a combination
    //     // of both an observer and observable.
    //     return Subject.create(observer, observable);
    // }
}
