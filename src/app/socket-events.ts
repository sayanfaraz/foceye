export enum SocketEvent {
    DATA_EVENT = 'data_event',
    EYE_EVENT = 'eye_event'
}
