import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// import { ChartsModule } from 'ng2-charts';
// import 'chart.js/src/chart.js';
import "chartist/dist/chartist.js";

import { AppComponent } from './app.component';
import { BiofeebackComponent } from './biofeeback/biofeeback.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { DevGraphComponent } from './dev-graph/dev-graph.component';


@NgModule({
  declarations: [
    AppComponent,
    BiofeebackComponent,
    HomeComponent,
    DevGraphComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
